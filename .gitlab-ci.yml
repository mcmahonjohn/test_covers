# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages

# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Dart.gitlab-ci.yml

# https://hub.docker.com/r/google/dart
image: google/dart:2.7.2

# List of stages for jobs, and their order of execution
stages:
  - dependencies
  - build
  - test

variables:
  # Use to learn more, go to https://dart.dev/tools/dart-test
  PUB_VARS: "--platform vm"

default:
  before_script:
    - export PUB_CACHE=".pub-cache/"
    - export PATH="$PATH":"$PUB_CACHE/bin"

  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    when: 'on_success'
    paths:
      - .pub-cache/bin/
      - .pub-cache/global_packages/
      - .pub-cache/hosted/
      - .dart_tool/
      - .packages
      - test/stub_package/.dart_tool/
      - test/stub_package/.packages

install-dependencies-job:
  stage: dependencies
  script:
    - pub get --no-precompile

build-job:
  stage: build
  needs:
    - install-dependencies-job
  script:
    - pub get --precompile

unit-test-job:
  stage: test
  needs:
    - build-job
  script:
    - pub run test $PUB_VARS

lint-test-job:
  stage: test
  needs:
    - install-dependencies-job
  script:
    - dartanalyzer --fatal-infos --fatal-warnings .
  allow_failure: true

format-test-job:
  stage: test
  needs:
    - install-dependencies-job
  script:
    - dartfmt -n --set-exit-if-changed lib/
  allow_failure: true
